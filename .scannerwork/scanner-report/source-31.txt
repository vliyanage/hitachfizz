package com.virtusa.flex.insight.utils;

import com.virtusa.flex.insight.InsightDataValidatorApplication;
import com.virtusa.flex.insight.api.commons.ApiRegistry;
import com.virtusa.flex.insight.api.commons.RequestOperation;
import com.virtusa.flex.insight.api.sonar.SonarApiViolationResponse;
import com.virtusa.flex.insight.api.sonar.SonarQubeClient;
import com.virtusa.flex.insight.dataSourceConfigurator.InsightDataSourceConfigurer;
import com.virtusa.flex.insight.extractor.DbDataExtractor;
import com.virtusa.flex.insight.model.Request;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

public class MetricAnalyzer {
    private static InsightDataSourceConfigurer insightDataSourceConfigurer = new InsightDataSourceConfigurer();
    Map<String, Map<String,String>> dataCOuntMap = new LinkedHashMap<>();
    Map<String,String> branchIdsFromDataMap = new LinkedHashMap<>();
    Map<String, String> branchIdsMap = new LinkedHashMap<>();
    Map<String,String> codeBranchFullName = new HashMap<>();

    public Map<String, Map<String,String>>  metricValuesFromDiffDataSources(String metric) throws IOException, URISyntaxException {
            switch (metric){
                case "OPEN_VIOLATION":
                    findOpenVIolationsFromApi();
                    findViolationDataInSonarData();
                    findViolationDataInDeepInsight();
                    break;
            }

        return dataCOuntMap;
    }

    private void findViolationDataInSonarData(){
        DbDataExtractor de = new DbDataExtractor(getJdbcTemplate("sonardata"));
        Map<String,String> sonarData = new LinkedHashMap<>();

            dataCOuntMap.put("SONARDB",de.findSonarDataViolationsCount(branchIdsFromDataMap,branchIdsMap));

    }

    private void findViolationDataInDeepInsight(){
        DbDataExtractor de = new DbDataExtractor(getJdbcTemplate("sonardata"));
        dataCOuntMap.put("DEEP-INSIGHT",de.findDeepInsightViolationsCount(branchIdsFromDataMap,branchIdsMap,codeBranchFullName));

    }

    private   Map<String, String> findOpenVIolationsFromApi() throws IOException, URISyntaxException {
        String timeInMills = String.valueOf( InsightUtils.currentMonthYearInMills());
        Map<String, String> apiResponse = new HashMap<>();

        //violation meta data values assign API

        DbDataExtractor dbDataExtractor = new DbDataExtractor(getJdbcTemplate("sonardata"));
         List<Map<String, Object>> result  = dbDataExtractor.getSonarAnalysisValue(timeInMills);

        for (Map<String, Object> s: result){

            //for (String d: s.keySet()){

//                if (String.valueOf(s.get("min(from_date_time)")).equals("0")){
//                   // dbDataExtractor.findMaxSonarFailedAnalysisDate(String.valueOf(s.get("module_id")))
//                    branchIdsFromDataMap.put(String.valueOf(s.get("module_id")) ,String.valueOf( branchIdsFromDataMap.put(String.valueOf(s.get("module_id")) ) );
//
//                }else {
                    branchIdsFromDataMap.put(String.valueOf(s.get("module_id")) ,String.valueOf(s.get("min(from_date_time)")) );


        }
            branchIdsMap  = dbDataExtractor.getSonarBranchNames(queryParamsBuilder(branchIdsFromDataMap.keySet()));

        for (String s: branchIdsMap.keySet()){
            String fullName = branchIdsMap.get(s);
            codeBranchFullName.put(s, fullName);
            String component = fullName.substring(0,fullName.lastIndexOf("-"));
            String branch = fullName.substring(fullName.lastIndexOf("-")+1);
            Request request = new Request(ApiRegistry.SONAR_API_ISSUES, RequestOperation.GET,component,branch,InsightUtils.convertTimestampToDate(Long.valueOf(branchIdsFromDataMap.get(s))));

            System.out.println(request);

            SonarQubeClient sq = new SonarQubeClient();
            SonarApiViolationResponse response = (SonarApiViolationResponse) sq.invoke(request);
            if (response.getJsonObject()!=null){
                apiResponse.put(fullName,String.valueOf(response.getJsonObject().get("issues").getAsJsonArray().size()));
            } else {
                apiResponse.put(fullName,"NO VIOLATION RECORDS FOUND ");
            }

        }

        dataCOuntMap.put("API",apiResponse);


        return apiResponse;

    }

    private  JdbcTemplate getJdbcTemplate(String dbName){
        String userName = (String) InsightDataValidatorApplication.configs.get("username");
        String password = (String) InsightDataValidatorApplication.configs.get("password");
        String url = ((String) InsightDataValidatorApplication.configs.get("db_url")).concat(dbName).concat("?useSSL=false");
        String driverClassName= "com.mysql.jdbc.Driver";
        DriverManagerDataSource datasource = insightDataSourceConfigurer.createTargetDataSources(driverClassName,url,userName,password);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);
        return jdbcTemplate;
    }

    private String queryParamsBuilder(Set<String> fields){
        StringBuilder sb = new StringBuilder();
        for (String s : fields){

            sb.append(s).append(",");
        }
        String fieldsValues = sb.substring(0,sb.lastIndexOf(",")).toString();
        System.out.println("sb "+sb);
        return fieldsValues;
    }



}
