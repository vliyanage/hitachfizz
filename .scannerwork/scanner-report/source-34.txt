<!DOCTYPEhtmlPUBLIC"-//W3C//DTDHTML4.01Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <metahttp-equiv
    ="Content-Type"content="text/html;charset=UTF-8"><title>Open Violation</title>
    <link rel="stylesheet" href="paleBlueRows.css">
    <h3>$heading</h3>
</head>
<body>
<div>
    <table class="paleBlueRows">
        <thead>
        <tr>
            <th>data source</th>

            <th>violation</th>

            <th>deleted_line</th>

            <th>added_line</th>

            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>API</td>

            <td>1</td>

            <td>2</td>

            <td>3</td>
        </tr>
        <tr>
            <td>Deep Insight</td>

            <td>1</td>

            <td>2</td>

            <td>3</td>
        </tr>
        <tr>
            <td>Fact table</td>

            <td>1</td>

            <td>2</td>

            <td>3</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>