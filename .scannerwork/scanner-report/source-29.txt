package com.virtusa.flex.insight.service;

import com.virtusa.flex.insight.InsightDataValidatorApplication;
import com.virtusa.flex.insight.utils.MetricAnalyzer;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class ReportGenerator {

    public void createRport() throws Exception {
        //load from yaml
        String status = null;
        String metric="OPEN_VIOLATION";
        String project = (String) InsightDataValidatorApplication.configs.get("project");
        String branch = (String)InsightDataValidatorApplication.configs.get("branch");


        //fields need to build open violation
        Map<String,Map<String,String>> dataMap = new LinkedHashMap<>();
        String branchName = InsightDataValidatorApplication.configs.get("project").toString().concat("-").concat(InsightDataValidatorApplication.configs.get("branch").toString());

        //get data from API
        MetricAnalyzer ma = new MetricAnalyzer();
        Map<String, Map<String,String>> dataCOuntMap = ma.metricValuesFromDiffDataSources(InsightDataValidatorApplication.configs.get("report").toString().toUpperCase());

        dataMap.put("API", dataCOuntMap.get("API"));
        dataMap.put("DEEP-INSIGHT",dataCOuntMap.get("DEEP-INSIGHT"));
        dataMap.put("SONARDB",dataCOuntMap.get("SONARDB"));

        Set<String> headers = new LinkedHashSet<>();
        headers.addAll(dataCOuntMap.get("DEEP-INSIGHT").keySet());
        headers.add("Data Source");


        String content=null;
        {
            try {
                content = new String(Files.readAllBytes(Paths.get("../template/template.html")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        String title = metric;
        StringBuilder sb=new StringBuilder(content);

        int theadIndexStart = sb.indexOf("<tr>");
        Stack<String> stck = new Stack<>();
        for (String s: headers){
            System.out.println(s.length());
            stck.push(s);
            String  str = "\n".concat("<th>").concat(s).concat("</th>").concat("\n");
            sb.insert(theadIndexStart+4,str);
        }
        StringBuilder sb2 = new StringBuilder();


        for (String s: dataMap.keySet()){

            String str2 =  "<tr>".concat("\n").concat("<td>").concat(s).concat("</td>").concat("\n");
            sb2.append(str2);
            int size = stck.size()-2;
            while (size>=0){
                System.out.println(stck.elementAt(size));
                String str3;
                    str3 =  "\n".concat("<td>").concat(dataMap.get(s).get(stck.elementAt(size))).concat("</td>").concat("\n");
                size--;
                sb2.append(str3);
            }
            sb2.append("</tr>");

        }
        int bodyIndex = sb.indexOf("<tbody>");
        sb.insert(bodyIndex+7,sb2);
        File newHtmlFile = new File("../reports/open-violation.html");
        try {
            FileUtils.writeStringToFile(newHtmlFile,sb.toString().replace("$heading","OPEN VIOLATIONS"), Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
