package com.virtusa.flex.insight.api;

import com.virtusa.flex.insight.model.Request;

import java.io.IOException;
import java.net.URISyntaxException;

public interface ApiClient {

    Auth authenticate(String apiKey);
    RequestPayload buildRequestPayload(Request request,Auth authResult);
    ApiResponse invokeApi(RequestPayload requestPayload,Transport transport,Auth auth,Request request) throws IOException, URISyntaxException;
    Transport configureTransport(Request request);

    default ApiResponse invoke(Request request) throws IOException, URISyntaxException {
        Auth auth = authenticate(request.getApiKey());
        Transport transport = configureTransport(request);
        RequestPayload requestPayload=buildRequestPayload(request,auth);
        return invokeApi(requestPayload,transport,auth,request);
    }

}
