package com.virtusa.flex.insight.api.sonar;

import com.virtusa.flex.insight.api.RequestPayload;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
@Data
public class SonarRequestPayload implements RequestPayload {
    Map<String, Object> queryParams = new HashMap<>();
}
