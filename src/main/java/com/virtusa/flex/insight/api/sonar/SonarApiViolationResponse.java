package com.virtusa.flex.insight.api.sonar;

import com.google.gson.JsonObject;
import com.virtusa.flex.insight.api.ApiResponse;
import lombok.Data;


@Data
public class SonarApiViolationResponse implements ApiResponse {
    private JsonObject jsonObject;
}
