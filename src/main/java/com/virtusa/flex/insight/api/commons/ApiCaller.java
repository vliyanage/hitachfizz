package com.virtusa.flex.insight.api.commons;

import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class ApiCaller {
    HttpClient client;

    public ApiCaller() {
        client = HttpClientBuilder.create()
                .build();
    }

    public JsonObject callApi(final String requestUrl, final Map<String, Object> queryParams) throws IOException, URISyntaxException{

        JsonParser parser = new JsonParser();
        URIBuilder builder = new URIBuilder(StringUtils.appendIfMissing("http://era/sonar", "/") + requestUrl);
        for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
            builder.setParameter(entry.getKey(), entry.getValue().toString());
        }
        builder.setParameter("ps", "500");
        URI uri = builder.build();

        HttpGet httpget = new HttpGet(uri);

        System.out.println(" url ===>>> "+ uri);
        byte[] encodedAuth = Base64.getEncoder().encode(("admin" + ":" + "1234qwer$").getBytes(StandardCharsets.UTF_8));
        httpget.setHeader("Authorization", "Basic " + new String(encodedAuth, StandardCharsets.UTF_8));
        HttpResponse response = client.execute(httpget);

        JsonObject result = null;

        if (response.getStatusLine().getStatusCode() == 200) {
            String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");

            try {
                result = parser.parse(responseString).getAsJsonObject();
            } catch (JsonSyntaxException e) {
                JsonReader reader = new JsonReader(new StringReader(responseString));
                reader.setLenient(true);
                result = parser.parse(reader).getAsJsonObject();

            }
        } else {
            String errMsg = "The status received from " + requestUrl + " is " + response.getStatusLine().getStatusCode() + "(" + response.getStatusLine().getReasonPhrase() + ")";
            //throw new Exception(errMsg);
        }

        return result;
    }
}
