package com.virtusa.flex.insight.api.sonar;

import com.virtusa.flex.insight.api.*;
import com.virtusa.flex.insight.api.commons.ApiCaller;
import com.virtusa.flex.insight.api.commons.ApiReference;
import com.virtusa.flex.insight.api.commons.ApiRegistry;
import com.virtusa.flex.insight.model.Request;
import com.virtusa.flex.insight.model.sonar.AuthResult;
import okio.ByteString;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class SonarQubeClient implements ApiClient {

    @Override
    public Auth authenticate(String apiKey) {
        //add userName Password
        String usernameAndPassword = "" + ":" + "$";
        String encoded = ByteString.encodeString(usernameAndPassword,  Charset.forName("ISO-8859-1")).base64();
        System.out.println("base46====>>>>" + encoded);
        AuthResult auth = new AuthResult();
        auth.setAuthorizationKey("Basic " + encoded);
        return  auth;
    }

    @Override
    public RequestPayload buildRequestPayload(Request request, Auth authResult) {
        SonarRequestPayload sonarRequestPayload;
        switch (request.getApiKey()){
            case (ApiRegistry.SONAR_API_MEASURE_CODE_COVERAGE):
                sonarRequestPayload = setCodeCoverageQueryParams(request);
                break;
            case (ApiRegistry.SONAR_API_ISSUES):
                sonarRequestPayload = setViolationQueryParams(request);
                break;


            default:
                throw new IllegalStateException("Unexpected value: " + request.getApiKey());
        }
        return sonarRequestPayload;
    }

    @Override
    public ApiResponse invokeApi(RequestPayload requestPayload, Transport transport, Auth auth, Request request) throws IOException, URISyntaxException {

        ApiCaller apiCaller = new ApiCaller();
        SonarRequestPayload sonarRequestPayload = (SonarRequestPayload) requestPayload;
        JsonObject jsonObject;
        switch (request.getApiKey()){
            case (ApiRegistry.SONAR_API_ISSUES):
               SonarApiViolationResponse res = new SonarApiViolationResponse();
               jsonObject = apiCaller.callApi(ApiReference.SONAR_API_ISSUES,sonarRequestPayload.getQueryParams());
               res.setJsonObject(jsonObject);
               return res;

            default:
                throw new IllegalStateException("Unexpected value: " + request.getApiKey());
        }

    }

    @Override
    public Transport configureTransport(Request request) {
        return null;
    }

    private SonarRequestPayload setCodeCoverageQueryParams(Request request){
       SonarRequestPayload requestPayload = new SonarRequestPayload();

        return requestPayload;

    }
    private SonarRequestPayload setViolationQueryParams(Request request){
        SonarRequestPayload requestPayload = new SonarRequestPayload();
        Map<String, Object> queryParams = new HashMap<>();

        queryParams.put("componentKeys",request.getComponent());
        queryParams.put("branch",request.getBranch());
        queryParams.put("createdAfter", request.getCreatedAfter());
        queryParams.put("status","OPEN");

        requestPayload.setQueryParams(queryParams);
        return requestPayload;

    }
}
