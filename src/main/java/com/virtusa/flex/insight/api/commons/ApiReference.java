package com.virtusa.flex.insight.api.commons;

public interface ApiReference {
    String SONAR_API_MEASURE = "api/measures/component";
    String SONAR_API_ISSUES =  "api/issues/search";

}
