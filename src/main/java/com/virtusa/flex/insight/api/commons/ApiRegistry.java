package com.virtusa.flex.insight.api.commons;

public interface ApiRegistry {
    String SONAR_API_MEASURE_CODE_COVERAGE = "CODE_COVERAGE";
    String SONAR_API_ISSUES = "OPEN_VIOLATION";
}
