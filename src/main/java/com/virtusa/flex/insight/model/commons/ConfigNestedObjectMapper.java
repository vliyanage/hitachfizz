package com.virtusa.flex.insight.model.commons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Map;

@Data
public class ConfigNestedObjectMapper {
    private Map<String,Map<String,String>> dashboardsMap;


    public Map<String, Map<String, String>> getDashboardsMap() {
        return dashboardsMap;
    }

    public void setDashboardsMap(Map<String, Map<String, String>> dashboardsMap) {
        this.dashboardsMap = dashboardsMap;
    }
}
