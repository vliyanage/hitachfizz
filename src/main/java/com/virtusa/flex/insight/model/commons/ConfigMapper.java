package com.virtusa.flex.insight.model.commons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Map;

@Data
public class ConfigMapper {
    private Map<String,ConfigNestedObjectMapper> database;

}
