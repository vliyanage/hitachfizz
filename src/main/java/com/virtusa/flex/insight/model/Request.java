package com.virtusa.flex.insight.model;

import lombok.Data;

@Data
public class Request {
    String apiKey;
    String requestOperation;
    String component;
    String branch;
    String createdAfter;

    public Request(String apiKey, String requestOperation, String component, String branch, String createdAfter) {
        this.apiKey = apiKey;
        this.requestOperation = requestOperation;
        this.component = component;
        this.branch = branch;
        this.createdAfter = createdAfter;
    }
}
