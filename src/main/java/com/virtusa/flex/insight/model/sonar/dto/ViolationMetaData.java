package com.virtusa.flex.insight.model.sonar.dto;

import lombok.Data;

@Data
public class ViolationMetaData {
    String codingRule;
    String codingRuleType;
    String endLine;
}
