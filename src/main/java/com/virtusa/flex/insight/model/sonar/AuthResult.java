package com.virtusa.flex.insight.model.sonar;

import com.virtusa.flex.insight.api.Auth;
import lombok.Data;

@Data
public class AuthResult implements Auth {
    String authorizationKey;
}
