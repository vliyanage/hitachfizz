package com.virtusa.flex.insight.metric;

public interface MetricsRawDataMapper {


    String OPEN_VIOLATION[] = {"Severity","Message","Component"};
    String OPEN_VULNERABILITIES[] = {"added_line","deleted_line","violation"};
}
