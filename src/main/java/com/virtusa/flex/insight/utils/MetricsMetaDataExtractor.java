package com.virtusa.flex.insight.utils;

import com.virtusa.flex.insight.metric.MetricsRawDataMapper;

public class MetricsMetaDataExtractor {

    public  static  Object getMetricsMetaData(String metric) throws Exception {
        switch (metric){
            case "OPEN_VIOLATION":
                return MetricsRawDataMapper.OPEN_VIOLATION;
            case "OPEN_VULNERABILITIES":
                return MetricsRawDataMapper.OPEN_VULNERABILITIES;

            default:
                throw new Exception("Invalid Metric");
        }

    }
}
