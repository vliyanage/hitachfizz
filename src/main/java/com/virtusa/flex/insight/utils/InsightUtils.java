package com.virtusa.flex.insight.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class InsightUtils {
    public static String passwordEncoder(){
        return null;
    }
    public static Object  currentMonthYearInMills(){

        LocalDate currentdate = LocalDate.now();
        System.out.println("Current date: "+currentdate.toString());


        ZonedDateTime zone = ZonedDateTime.now(ZoneId.of("Etc/UTC"));

        String myDate = currentdate.toString().substring(0,8).concat("01") + " 00:00:00";
        LocalDateTime localDateTime = LocalDateTime.parse(myDate,
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss") );

        return ZonedDateTime.of(localDateTime, ZoneId.of("Etc/UTC")).toInstant().toEpochMilli();
    }

    public static  String convertTimestampToDate(Long milisecond){
        Date date = new Date(milisecond);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("long value " + milisecond);
        System.out.println("date===>>>"+ df.format(date));
        return df.format(date);
    }
    public static  String findFaildAnalysisFromDate(Long milisecond){
        Date date = new Date(milisecond);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String myDate = df.format(date) + " 00:00:00";
        LocalDateTime localDateTime = LocalDateTime.parse(myDate,
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss") );
        System.out.println("long value " + milisecond);
        return String.valueOf(ZonedDateTime.of(localDateTime, ZoneId.of("Etc/UTC")).toInstant().toEpochMilli());
    }


    /**
     *
     * SimpleDateFormat dateSonar = new SimpleDateFormat("yyyy-MM");
     * issuesList.forEach(jObj ->
     *         {​​​​​
     *             try {​​​​​
     *                 if (((JSONObject) jObj).get("author").toString().equals(sonarQube.getAuthor()) && ((JSONObject) jObj).get("status").toString().equals("OPEN") && dateSonar.parse(String.valueOf(((JSONObject) jObj).get("updateDate")).substring(0, 7)).equals("2020-12")) {​​​​​
     *                     list.add(((JSONObject) jObj).get("status").toString());
     *                 }​​​​​
     *             }​​​​​ catch (java.text.ParseException e) {​​​​​
     *                 e.printStackTrace();
     *             }​​​​​
     *         }​​​​​
     * );
     *
     *
     */
}
