package com.virtusa.flex.insight.extractor;

import com.virtusa.flex.insight.InsightDataValidatorApplication;
import com.virtusa.flex.insight.dataSourceConfigurator.InsightDataSourceConfigurer;
import com.virtusa.flex.insight.model.sonar.dto.ViolationMetaData;
import com.virtusa.flex.insight.query.SonarDBQuery;
import com.virtusa.flex.insight.utils.InsightUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DbDataExtractor {
    private static InsightDataSourceConfigurer insightDataSourceConfigurer = new InsightDataSourceConfigurer();
    JdbcTemplate jdbcTemplate;
    String branch=null;
    String parameter[];

    public DbDataExtractor(String branch,String params[]) {
        this.branch = branch;
        this.parameter= params;

    }

    public DbDataExtractor(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //fetch sonardb value for give parameter
    public List<Map<String, Object>> getSonarValue(){
        ViolationMetaData violationMetaData = new ViolationMetaData();
        jdbcTemplate = getJdbcTemplate("sonardata");
        String queryForBranch=null;
        if (branch==null){
            queryForBranch = "develop";
        }

        String branch_id = (String) jdbcTemplate.queryForList(queryForBranch).get(0).get("code_branch_id");

        String query = "SELECT " +  queryParamsBuilder(parameter) + " FROM violatio where code_branch = " + "'" + branch +("'");

        System.out.println("query 1 ======>>> " + query);
        List<Map<String, Object>> result = new ArrayList<>();
        try {
             result = jdbcTemplate.queryForList(query);
            //set values to dto


            System.out.println(" line===2" + result.size());
        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }

    //fetch generic_dashboard value for give parameter
    public ViolationMetaData getGenericDashboardValue(){
        jdbcTemplate = getJdbcTemplate("generic_dashboard");
        ViolationMetaData violationMetaData = new ViolationMetaData();

        String query = "SELECT " +  queryParamsBuilder(parameter) + " FROM insightlive_violation_deep_insight where code_branch = " + "'" + branch +("'");
        System.out.println("query 2 ======>>> " + query);
        try {
            List<Map<String, Object>> result = jdbcTemplate.queryForList(query);
            //set values to dto


            System.out.println(" line===5" + result.size());
        }catch (Exception e){
            e.printStackTrace();
        }

        return violationMetaData;

    }


    public List<Map<String, Object>> getSonarAnalysisValue(String time){
        jdbcTemplate = getJdbcTemplate("sonardata");

        String query = SonarDBQuery.FIND_BRANCH_ID_FOR_THIS_MONTH_DATA_1+ " " + time + " " +SonarDBQuery.FIND_BRANCH_ID_FOR_THIS_MONTH_DATA_2 + " " + time + " "+
                SonarDBQuery.FIND_BRANCH_ID_FOR_THIS_MONTH_DATA_3;
        System.out.println("query 1 ======>>> " + query);
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            result = jdbcTemplate.queryForList(query);
            //set values to dto


            System.out.println(" line===2" + result.size());
        }catch (Exception e){
            e.printStackTrace();
        }

        return result;

}
    public Map<String, String> getSonarBranchNames(String ids){
        jdbcTemplate = getJdbcTemplate("sonardata");

        String query = SonarDBQuery.FIND_BRANCH  + ids + " ) ";
        System.out.println("query 1 ======>>> " + query);
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            result = jdbcTemplate.queryForList(query);
            //set values to dto


            System.out.println(" line===2" + result.size());
        }catch (Exception e){
            e.printStackTrace();
        }
        Map<String, String> branchIdsMap = new LinkedHashMap<>();
        for (Map<String, Object> s: result){
               branchIdsMap.put(String.valueOf(s.get("code_branch_id")),(String) s.get("name"));

        }

        return branchIdsMap;

    }

    public String findMaxSonarFailedAnalysisDate(String module_id){
        jdbcTemplate = getJdbcTemplate("sonardata");
        String query = "select max(to_date_time) from sonardata.analysis where module_id=" +  module_id +  " and is_success=0";
        String date = String.valueOf(jdbcTemplate.queryForList( query).get(0).get("max(to_date_time)"));
        String fromAnalysisDate = InsightUtils.findFaildAnalysisFromDate(Long.valueOf(date));
        return fromAnalysisDate;

    }



    private  JdbcTemplate getJdbcTemplate(String dbName){
        String userName = (String) InsightDataValidatorApplication.configs.get("username");
        String password = (String) InsightDataValidatorApplication.configs.get("password");
        String url = ((String) InsightDataValidatorApplication.configs.get("db_url")).concat(dbName).concat("?useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        String driverClassName= "com.mysql.jdbc.Driver";
        DriverManagerDataSource datasource = insightDataSourceConfigurer.createTargetDataSources(driverClassName,url,userName,password);

        jdbcTemplate = new JdbcTemplate(datasource);
        return jdbcTemplate;
    }

    private String queryParamsBuilder(String fields[]){
        StringBuilder sb = new StringBuilder();
        for (String s : fields){

            sb.append(s).append(",");
        }
        String fieldsValues = sb.substring(0,sb.lastIndexOf(",")).toString();
        System.out.println("sb "+sb);
        return fieldsValues;
    }

    public Map<String, String> findSonarDataViolationsCount(Map<String,String> branchIdsFromDataMap , Map<String, String> branchIdsMap ){
        jdbcTemplate = getJdbcTemplate("sonardata");
        Map<String, String> resultDataMap = new LinkedHashMap<>();
        for (String s: branchIdsFromDataMap.keySet()){
            String query = "select count(violation_id) as violation_count from violation where module_id=" + s + " and status='OPEN' and created_timestamp > " + branchIdsFromDataMap.get(s);
            jdbcTemplate.queryForList(query);

            resultDataMap.put(branchIdsMap.get(s),String.valueOf(jdbcTemplate.queryForList(query).get(0).get("violation_count")));
        }
        return resultDataMap;
    }

    public Map<String, String> findDeepInsightViolationsCount(Map<String,String> branchIdsFromDataMap , Map<String, String> branchIdsMap,Map<String,String> codeBranchFullNames ){
        Map<String, String> resultDataMap = new LinkedHashMap<>();
        jdbcTemplate = getJdbcTemplate("generic_dashboard");
        for (String s: branchIdsFromDataMap.keySet()){
            String queryForGetSubmissionDate = "SELECT last_submitted_time FROM sonardata.code_branch_submission_meta where code_branch_id= " + s + " AND last_submission_status='SUCCESS'";
            String lastSubmitDate = jdbcTemplate.queryForList(queryForGetSubmissionDate).get(0).get("last_submitted_time").toString();


            String queryForDeepInsightData = "SELECT count(violation_id) FROM generic_dashboard.insightlive_violation_deep_insight where code_branch='" + codeBranchFullNames.get(s)+
                    "' and timestamp>" +  lastSubmitDate + " and status='OPEN' and is_baseline=0";

            resultDataMap.put(branchIdsMap.get(s),String.valueOf(jdbcTemplate.queryForList(queryForDeepInsightData).get(0).get("count(violation_id)")));
        }
        return resultDataMap;
    }

}
