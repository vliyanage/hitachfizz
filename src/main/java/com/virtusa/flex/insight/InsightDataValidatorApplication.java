package com.virtusa.flex.insight;

import com.virtusa.flex.insight.service.ReportGenerator;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class InsightDataValidatorApplication {

    public static Map<String,Object> configs = new HashMap<>();

    public static void main( String[] args ) throws Exception {




//        ReportGenerator reportGenerator = new ReportGenerator();
//        Map<String, Object> yamlData = loadConfigurationData();
//        setConfigs(yamlData);
//
//        reportGenerator.createRport();

    }
    private static Map<String, Object> loadConfigurationData(){

        Yaml yaml = new Yaml();
        InputStream input = null;
        try {
            input = new FileInputStream("../config/config.yaml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Map<String, Object> obj = yaml.load(input);
        return obj;

    }
    private static void setConfigs(Map<String, Object> yamlData){
        for (String s: yamlData.keySet()){
            configs.put(s,yamlData.get(s));
        }
    }

}
