package com.virtusa.flex.insight.service;

import com.virtusa.flex.insight.model.sonar.dto.ViolationMetaData;

import java.util.LinkedHashMap;
import java.util.Map;

public class DataSourceResultMapper {

    public Map<String,String> dataMapperOpenViolation( ViolationMetaData violationMetaData){
        Map<String,String> sonarDataMapper = new LinkedHashMap<>();
        sonarDataMapper.put("end_line",violationMetaData.getEndLine());
        sonarDataMapper.put("coding_rule_type", violationMetaData.getCodingRuleType());
        sonarDataMapper.put("coding_rule",violationMetaData.getCodingRule());


        return sonarDataMapper;
    }
}
