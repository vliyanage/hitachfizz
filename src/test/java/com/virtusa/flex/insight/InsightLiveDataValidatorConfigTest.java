package com.virtusa.flex.insight;

import com.virtusa.flex.insight.model.sonar.dto.ViolationMetaData;
import com.virtusa.flex.insight.service.DataSourceResultMapper;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class InsightLiveDataValidatorConfigTest  extends TestCase {

    public InsightLiveDataValidatorConfigTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( InsightDataValidatorApplicationTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    public void testRule(){
        DataSourceResultMapper dataSourceResultMapper = new DataSourceResultMapper();
        ViolationMetaData vm = new ViolationMetaData();

        vm.setEndLine("10");
        vm.setCodingRuleType("type");
        vm.setCodingRule("rule");

        assertEquals("rule",dataSourceResultMapper.dataMapperOpenViolation(vm).get("coding_rule"));
    }

    public void testType(){
        DataSourceResultMapper dataSourceResultMapper = new DataSourceResultMapper();
        ViolationMetaData vm = new ViolationMetaData();

        vm.setEndLine("10");
        vm.setCodingRuleType("type");
        vm.setCodingRule("rule");

        assertEquals("type",dataSourceResultMapper.dataMapperOpenViolation(vm).get("coding_rule_type"));
    }
}
