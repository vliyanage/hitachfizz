package com.virtusa.flex.insight;

import com.virtusa.flex.insight.dataSourceConfigurator.InsightDataSourceConfigurer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class InsightDataValidatorApplicationTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public InsightDataValidatorApplicationTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( InsightDataValidatorApplicationTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testConfigurationObject(){
        InsightDataSourceConfigurer isc = new InsightDataSourceConfigurer();
        assertNotNull(isc.createTargetDataSources("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/", "test", "1234"));
    }
	
	public void testRule(){
        InsightDataSourceConfigurer GETDigit = new InsightDataSourceConfigurer();
        assertEquals(25,GETDigit.getDigit(25));
    }
}
